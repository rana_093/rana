package net.therap;

import javax.sound.sampled.Line;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.sql.Time;
import java.util.*;
import java.util.regex.*;

public class LogParser {
    public static void main(String[] args) throws FileNotFoundException {
        File ParserFile = new File("../log-parser-sample-file.log");
        Scanner MyReader = new Scanner(ParserFile);

        String TimeRegex = "\\d{2}:\\d{2}:\\d{2},\\d{3}";
        Pattern TimePattern = Pattern.compile(TimeRegex);

        String URLRegex = "URI=[^,]+";
        Pattern URLPattern = Pattern.compile(URLRegex);

        String GetRegex = "G,";
        Pattern GetPattern = Pattern.compile(GetRegex);

        String PostRegex = "P,";
        Pattern PostPattern = Pattern.compile(PostRegex);

        String ResponseRegex = "time=\\d+";
        Pattern ResponsePattern = Pattern.compile(ResponseRegex);

        HashMap< Integer, Integer > GetCount = new HashMap<>();
        HashMap< Integer, Integer> PostCount = new HashMap<>();

        HashMap< String , Integer > URLCount = new HashMap<>();
        HashMap< Integer , Set< String > > ListOfURL = new HashMap<>();
        HashMap< Integer, Integer > ResponseTime = new HashMap<>();
        Set< Integer > AllHours = new HashSet<>();

        while(MyReader.hasNextLine()){
            String LineData = MyReader.nextLine();

            boolean get = (LineData.indexOf("G,") != -1) ;
            boolean post = (LineData.indexOf("P,") != -1) ;
            if( !get && !post) { continue; }

            Matcher Time = TimePattern.matcher(LineData);

            int Hour = -1;
            if(Time.find()){
                Hour = Integer.parseInt(Time.group(0).substring(0, 2));
            }
            if(Hour!=-1){ AllHours.add(Hour); }
            if(get){
                GetCount.put(Hour, GetCount.containsKey(Hour) ? GetCount.get(Hour) + 1 : 1);
            }else if(post){
                PostCount.put(Hour, PostCount.containsKey(Hour) ? PostCount.get(Hour) + 1 : 1);
            }


            Matcher URL = URLPattern.matcher(LineData);
            String MyURL = "";
            if(URL.find()){
                String temp = URL.group(0);
                int len = temp.length();
             //   System.out.println("Len: " + len);
                MyURL = temp.substring(5, len -1);
            }
            if(MyURL.length() > 0){
                if(URLCount.containsKey(MyURL)){
                    URLCount.put(MyURL, URLCount.get(MyURL) + 1);
                }else{
                    URLCount.put(MyURL, 1);
                }
            }

            if(ListOfURL.containsKey(Hour)){
                Set< String  > S = ListOfURL.get(Hour);
                S.add(MyURL);
                ListOfURL.put(Hour,S);
            }else{
                Set< String > S = new HashSet<>();
                S.add(MyURL);
                ListOfURL.put(Hour, S);
            }

            Matcher Response = ResponsePattern.matcher(LineData);
            if(Response.find()){
                String TempResponse = Response.group(0);
                int length_reponse = TempResponse.length();
                int Response_Time = Integer.parseInt(TempResponse.substring(5, length_reponse ));
                ResponseTime.put(Hour, ResponseTime.containsKey(Hour)? ResponseTime.get(Hour) + Response_Time : Response_Time);
            }
        }

        for(Integer hour: AllHours){
            int TotalGet = GetCount.get(hour);
            int TotalPost = PostCount.get(hour);
            int TotalResponseTime = ResponseTime.get(hour);
            int TotalUniqueURL = ListOfURL.get(hour).size();
            System.out.println("Start Time: " + hour + " , Total Get: " + TotalGet + " , Total Post: " + TotalPost + " ,  Total Response Time: " + TotalResponseTime + " , Total Unique URL: " + TotalUniqueURL);
        }

    }
}
